%% Programming Example 5.2.1 - Signal Quantization
% DigitalAudioTheory.com

fs=1000;    % sample rate (Hz)
T=4;        % duration (s)
n = 0:fs*T; % time vector
f=0.25;     % frequency (Hz)
A=1;        % Max Amplitude 
N=8;		% bit-depth
 
x=0.5*cos(2*pi*f*n/fs); % signal
x8 = quantBits(x,8,A);  % 8-bit quantized version of x 

figure; plot(x);
hold on; grid on;
plot(x8);
