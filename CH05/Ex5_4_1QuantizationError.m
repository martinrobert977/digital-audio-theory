%% Programming Example 5.4.1 - Quantization Error
% DigitalAudioTheory.com
fs=1000;    % sample rate (Hz)
T=4;        % duration (s)
n = 0:fs*T; % time vector

% plot quantization error
x=0.5*cos(2*pi*f*n/fs); % signal
x8 = quantBits(x,8,A);  % 8-bit quantized version of x 

eq_x8 = x-x8;
plot(eq_x8)

% plot quantized signal (scaled)
q=2/2^8;
hold on; grid on;
plot(x8*q, 'LineWidth', 3)
yticks([-q/2 -q/4 0 q/4 q/2]);
yticklabels({'-q/2', '-q/4', '0', '+q/4', '+q/2'});

