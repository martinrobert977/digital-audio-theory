%% Programming Example 8.6.1 - Pre-Echo
% DigitalAudioTheory.com

a_mp = firminphase(a_lp);
[x,fs] = audioread('castanets.wav');
sound(filter(a_lp,1,x),fs);
pause(6);
sound(filter(a_mp,1,x),fs);

grpdelay(a_mp,1,fs,fs); xlim([0 1000]);

