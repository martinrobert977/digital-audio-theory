%% Programming Example 8.4.1 - Time Delay Estimation
% DigitalAudioTheory.com

x=audioread('drum.wav');
w=audioread('drum_delay.wav');
 
 
[similarity,lag] = xcorr(w,x);
[~,I] = max(abs(similarity));
 
plot(lag,similarity)
title(sprintf('Delay of %f sec',lag(I)/44100))
 
