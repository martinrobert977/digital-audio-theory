%% Programming Example 7.6.1 - Filter Implementation
% DigitalAudioTheory.com

% create a delta input
x = [1; zeros(10,1)];
 
% delays for direct form
ynm1=0;
xnm1=0;
 
% single delay for canonical form
wnm1=0;
 
% execute the difference equation
for ii=1:length(x)
    % direct form equation
    y_df(ii) = 0.5*x(ii)+0.2*xnm1+0.1*ynm1;
    
    % set delays
    xnm1=x(ii);
    ynm1=y_df(ii);
    
    % canonical form equations
    w=x(ii)+0.1*wnm1;
    y_c(ii)=0.5*w+0.2*wnm1;
    
    % set single delay
    wnm1=w;
end

format long;
[y_df' y_c']

