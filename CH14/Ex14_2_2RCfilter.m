%% Programming Example 14.2.2 - RC Filter
% DigitalAudioTheory.com

fs=48000;
f=0:fs/2;
R=4.7e3;
C=10e-9;

% ha   = 1/(RC)*exp(-t/(RC))
% h[n] = 1/(RC)*exp(-n*Ts/(RC))
% H(z) = 1/(RC)/(1-exp(-Ts/RC)*z^-1)
 
% z transform
a=exp(-1/(R*C*fs));
[H, W] = freqz([1-a], [1 -a], 2*pi*f/fs);
 
% Laplace transform
Hc = freqs([1/(R*C)], [1 1/(R*C)], 2*pi*f);

semilogx(W/pi*fs/2, 20*log10(abs(Hc)));
hold on;
semilogx(W/pi*fs/2, 20*log10(abs(H)), '--');


