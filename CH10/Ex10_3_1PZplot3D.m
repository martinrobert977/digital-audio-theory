%% Programming Example 10.3.1 - 3D P/Z Plot
% DigitalAudioTheory.com

%%% z-plane grid
w = linspace(-pi, pi, 100);    % freq spacing
r = linspace(0,1,30);          % radius spacing
[wG,rG] = meshgrid(w,r);       % 2D Cartesian grid
 
% complex variable z (grid)
e=exp(1);
zG=rG.*e.^(j*wG);
 
%%% Calculate the function
% (3 different filters provided)
 
% 2nd-order FIR notch filter
%f= 5000;    % notch frequency
%Fs=44100;   % sample rate
%H=(zG-e^(j*2*pi*f/Fs)).*(zG-e^(-j*2*pi*f/Fs));
 
 
% 1st-order FIR LPF/HPF
% a1= -0.5;
% H = zG-a1;
 
% 2nd-order IIR resonator
 f=5000;
 Fs=44100;
 w0=2*pi*f/Fs;
 R=0.8
 H = 1./(zG.^2-2*R*cos(w0)*zG+R^2);
 
% extract magnitude and phase
mag = 20*log10(abs(H));
ang = unwrap(angle(H));
 
% create rectangular grid for 'surf' function
X = rG.*cos(wG);
Y = rG.*sin(wG);
surf(X,Y,mag, ang);
xlabel('Real')
ylabel('Imaginary')
zlabel('Level (dB)')
grid on
 
