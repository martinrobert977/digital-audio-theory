%% Programming Example 10.6.1 - 3-Band Parametric EQ
% DigitalAudioTheory.com

% proportional parametric equalizer
 
fs=44100;
 
% low-shelf
w_lo=2*pi*100/fs;    % 100 Hz
k_lo=db2mag(4);      % +4 dB
t_lo = tan(w_lo/2);
 
a0_lo=k_lo*t_lo+sqrt(k_lo);
a1_lo=k_lo*t_lo-sqrt(k_lo);
b0_lo=t_lo+sqrt(k_lo);
b1_lo=t_lo-sqrt(k_lo);
 
% mid band
c=cos(500*2*pi/fs);  % 500 Hz center
BW=2*pi*100/fs;      % 100 Hz bandwidth
k_mid=db2mag(-3);    % -3 dB
t_mid = tan(BW/2);
 
a0_mid=t_mid*sqrt(k_mid)+1;
a1_mid=-2*c;
a2_mid=-(t_mid*sqrt(k_mid)-1);
b0_mid=t_mid/sqrt(k_mid)+1;
b1_mid=-2*c;
b2_mid=-(t_mid/sqrt(k_mid)-1);
 
% hi-shelf
w_hi=2*pi*4000/fs;   % 4 kHz
k_hi=db2mag(1);      % +1 dB
t_hi = tan(w_hi/2);
 
a0_hi=sqrt(k_hi)*t_hi+k_hi;
a1_hi=sqrt(k_hi)*t_hi-k_hi;
b0_hi=sqrt(k_hi)*t_hi+1;
b1_hi=sqrt(k_hi)*t_hi-1;
 
% analyze coefficients
[H_HS, F] = freqz([a0_hi a1_hi], [b0_hi b1_hi], fs, fs);
H_LS = freqz([a0_lo a1_lo], [b0_lo b1_lo], fs, fs);
H_BP = freqz([a0_mid a1_mid a2_mid], [b0_mid b1_mid b2_mid], fs, fs);
 
% plot magnitude response
semilogx(F, 20*log10(abs(H_LS.*H_BP.*H_HS)))
grid on;
axis([20 20000 -12 12]);

