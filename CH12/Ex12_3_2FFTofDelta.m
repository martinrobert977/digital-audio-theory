%% Programming Example 12.3.2 - DFT of a Delta
% DigitalAudioTheory.com

N=8;
d = zeros(N,1);
d(1)=1;
 
plot(abs(fft(d,N)))
grid on; box on;
ylabel('Mag'); xlabel('k');

