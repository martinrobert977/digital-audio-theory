%% Programming Example 9.5.2 - Notch Filter
% DigitalAudioTheory.com

h=[1,-1,1];
den=1;
zplane(h,den)


% method 1 - define the magnitude response
 
Fs=44100;		% sampling rate
fdig = pi/3;	% notch location
f=0:Fs/2;		% frequencies to analyze
w=2*pi*f/Fs;	% digital omega
e=exp(1);		% e
z=1*e.^(j*w);		% z on the Unit Circle
 
% magnitude response
mag=abs(z-e^(j*fdig)).*abs(z-e^(-j*fdig));
plot(f, 20*log10(mag))
box on; grid on; xlabel('Freq (norm)'); ylabel('Mag (dB)'); title('Notch filter')
 
 



% method 2 - define the impulse response
h=[1,-1,1];		% freq. resp. numerator (same as IR)
den=1;		% freq. resp. denominator
Fs = 44100;
 
% return the complex freq. resp. and corresponding freq. array
[H,F]=freqz(h, den, Fs, Fs);
 
hold on;
plot(F, 20*log10(abs(H)), 'r--')

