%% Programming Example 3.3.1 - Simple Delay Effect
% DigitalAudioTheory.com
clear

% Load Audio
[x,fs]=audioread('snare.wav');
N=length(x);

% Effect Parameters
delay=0.2;        % seconds
k=round(delay*fs); % samples
b = 0.5;          % delay gain
 
for n=1:N+k
    if n>k && n-k<N
        xd=x(n-k,:);
    else
        xd=[0 0];
    end
    
    if n<N
        xn = x(n,:);
    else
        xn=[0 0];
    end
    
    y(n,:)=xn+b*xd;
end
 
soundsc(y,fs)

