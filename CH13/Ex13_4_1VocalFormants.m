%% Programming Example 13.4.1 - Vocal Formants
% DigitalAudioTheory.com

% capture audio
Fs=44100;
nbits=16;
nchans=1;
% if you are not recording on your default device, or channel 1, then use
% audiodevinfo to find the correct device ID and update the number of
% channels with nchans
vox=audiorecorder(Fs, nbits, nchans);
 
recordblocking(vox, 3);
 
play(vox);
 
x=getaudiodata(vox,'double');
N=2^17;
x=x(1:N);

%
% spectrogram
NFFT=4096;
overlap=NFFT/32;
figure()
% for Matlab use:
spectrogram(x, NFFT, overlap, NFFT, Fs, 'yaxis');
% for Octave use:
%specgram(x, NFFT, Fs);

%%
% define filter
Fn=Fs/2; 	%nyquist
ord=4; 	%order
fl=620; 	%hz
fh=920;
[a,b]=butter(ord, [fl/Fn, fh/Fn]); 
[H,W]=freqz(a,b,NFFT, 'whole');
 

% overlap and add
hannWin=hann(NFFT);
 
% make special first window and last window
firstWin = hannWin;
firstWin(1:end/2)=1;
lastWin=hannWin;
lastWin(end/2+1:end)=1;
 
% define output to match input size
y=zeros(size(x));
 
% how many frames?
numFrames = 2*length(x)/NFFT-1;
 
jump = NFFT/2;
 

for ii=1:numFrames
    % window selection
    if ii==1
        win=firstWin;
    elseif ii==numFrames
        win=lastWin;
    else
        win=hannWin;
    end
    
    %frame and window audio
    startIdx = (ii-1)*jump+1;
    endIdx   = startIdx+NFFT-1;
    
    frameBox = x(startIdx:endIdx);
    frame = frameBox.*win;
    
    % do spectral processing
    X=fft(frame,NFFT);
    FRAME=X.*H;
    frame=real(ifft(FRAME,NFFT));
    
    % overlap and add
    y(startIdx:endIdx) = y(startIdx:endIdx) + frame;
end
 
soundsc(y, Fs);

