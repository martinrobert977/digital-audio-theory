%% Programming Example 4.1.1 - Aliasing
% DigitalAudioTheory.com

Fs=10;      % sample rate = 10 Hz
T = 1;      % duration in s
n=[0:T*Fs]; % let's look at the first second
 
f1=2;       % x1 frequency 2Hz
f2=8;       % x2 frequency 8Hz
 
x1=sin(2*pi*f1*n/Fs);
x2=sin(2*pi*f2*n/Fs);
 
plot(n/Fs,x2);
grid on;
hold on; 
plot(n/Fs,x1);
 
% Let's also look at x2 sampled appropriately
Fs_hi=1000; % sample rate = 1000 Hz
n_hi=[0:T*Fs_hi]; 
xc=sin(2*pi*f2*n_hi/Fs_hi);
plot(n_hi/Fs_hi, xc, '--')

